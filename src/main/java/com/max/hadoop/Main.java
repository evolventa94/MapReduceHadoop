package com.max.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

    private static final Integer MIN_WORD_LENGTH = 7;

    private static final Counter counter = new Counters.Counter();
    static {

        //init counter
        counter.setValue(0);
    }

    static class WordLengthMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private String longetWord;
        // В методе инициализируем самое длинное слово (сперва пустая строка)
        @Override
        protected void setup(Context context) throws java.io.IOException, InterruptedException {
            longetWord = "";
        }

        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws java.io.IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            // Если находим слово длиннее - запоминаем
            Comparator<Integer> integerComparator = new Comparator<>(Integer.class);
            while (itr.hasMoreTokens()) {
                String next = itr.nextToken();
                if(next.length() < MIN_WORD_LENGTH) {
                    counter.increment(1);
                } else if(integerComparator.compare(next.length(), longetWord.length())) {
                    longetWord = next;
                }
            }
        }

        // Пишем в контекст самое длинное найденное слово
        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            String counterOutput = "count of words less than 7 length words: ";
            context.write(new Text(counterOutput), new IntWritable((int)counter.getValue()));
            context.write(new Text(longetWord), new IntWritable(longetWord.length()));
        }
    }

    static class WordLengthReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {

        protected void reduce(Text key, Iterable<Integer> values, Context context) throws java.io.IOException, InterruptedException {
            context.write(key, new IntWritable(key.getLength()));
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapred.textoutputformat.separatorText", ",");
        Job job = Job.getInstance(conf, "word length count");
        job.setJarByClass(Main.class);
        job.setMapperClass(WordLengthMapper.class);
        job.setCombinerClass(WordLengthReducer.class);
        job.setReducerClass(WordLengthReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

// Не придумал другого customType. Кроме как сравнивать тут ничего не приходится делать.
// Задачу вполне можно решить имеющимися средствами
// И про java.lang.Comparator<T> я знаю. Просто решил так. Нужен же был прям customType
class Comparator<T> {

    private Class clazz;

    public Comparator(Class<T> clazz) {
        this.clazz = clazz;
    }

    public boolean compare(T t1, T t2) {
        if(Comparable.class.isAssignableFrom(clazz)) {
            Comparable comparable1 = (Comparable) t1;
            Comparable comparable2 = (Comparable) t2;
            return comparable1.compareTo(comparable2) > 0;
        } else {
            throw new RuntimeException("Error while comparing objects");
        }
    }
}

