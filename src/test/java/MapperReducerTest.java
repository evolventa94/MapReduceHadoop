import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MapperReducerTest {

    private static final Integer MIN_WORD_LENGTH = 7;
    private static final Counter counter = new Counters.Counter();
    static {

        //init counter
        counter.setValue(0);
    }


    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;
    List<Text> fileByString = new ArrayList<>();

    @Before
    public void setUp() {
        File file = new File("src/test/resources/forTest.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            for(String line; (line = br.readLine()) != null; ) {
                fileByString.add(new Text(line));
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading file", e);
        }
        WordLengthMapper mapper = new WordLengthMapper();
        WordLengthReducer reducer = new WordLengthReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() {

        String longest = "loooooooooooooooooooooonnnnnnnnnnnnngeeeeeeeeeeeeeeessssssssst";
        StringJoiner stringJoiner = new StringJoiner();
        for (Text text : fileByString) {
            stringJoiner.append(text.toString());
        }
        mapDriver.withInput(new LongWritable(), new Text(stringJoiner.build()));
        mapDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() {
        String longest = "loooooooooooooooooooooonnnnnnnnnnnnngeeeeeeeeeeeeeeessssssssst";
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(longest.length()));
        reduceDriver.withInput(new Text(longest), values);
        reduceDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() {
        String longest = "loooooooooooooooooooooonnnnnnnnnnnnngeeeeeeeeeeeeeeessssssssst";
        StringJoiner stringJoiner = new StringJoiner();
        for (Text text : fileByString) {
            stringJoiner.append(text.toString());
        }
        mapReduceDriver.withInput(new LongWritable(), new Text(stringJoiner.build()));
        mapReduceDriver.withOutput(new Text(longest), new IntWritable(longest.length()));
        mapReduceDriver.runTest();
    }

    static class WordLengthMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        private String longetWord;
        // В методе инициализируем самое длинное слово (сперва пустая строка)
        @Override
        protected void setup(Context context) throws java.io.IOException, InterruptedException {
            longetWord = "";
        }

        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws java.io.IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());

            Comparator<Integer> integerComparator = new Comparator<>(Integer.class);
            // Если находим слово длиннее - запоминаем
            while (itr.hasMoreTokens()) {
                String next = itr.nextToken();
                if(next.length() < MIN_WORD_LENGTH) {
                    counter.increment(1);
                } else if(integerComparator.compare(next.length(), longetWord.length())) {
                    longetWord = next;
                }
            }
        }

        // Пишем в контекст самое длинное найденное слово
        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            String counterOutput = "count of words less than 7 length words: ";
            context.write(new Text(counterOutput), new IntWritable((int)counter.getValue()));
            context.write(new Text(longetWord), new IntWritable(longetWord.length()));
        }
    }

    static class WordLengthReducer extends
            Reducer<Text, IntWritable, Text, IntWritable> {

        protected void reduce(Text key, Iterable<Integer> values, Context context) throws java.io.IOException, InterruptedException {
            context.write(key, new IntWritable(key.getLength()));
        }
    }

}

// Не придумал другого customType. Задачу вполне можно решить имеющимися средствами
class Comparator<T> {

    private Class clazz;

    public Comparator(Class<T> clazz) {
        this.clazz = clazz;
    }

    public boolean compare(T t1, T t2) {
        if(Comparable.class.isAssignableFrom(clazz)) {
            Comparable comparable1 = (Comparable) t1;
            Comparable comparable2 = (Comparable) t2;
            return comparable1.compareTo(comparable2) > 0;
        } else {
            throw new RuntimeException("Error while comparing objects");
        }
    }
}

class StringJoiner {

    private String result;

    private String delimiter;

    private String prefix;

    private String suffix;

    public StringJoiner setDelimiter(String delimiter) {
        this.delimiter = delimiter;
        return this;
    }

    public StringJoiner setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public StringJoiner setSuffix(String suffix) {
        this.suffix = suffix;
        return this;
    }

    public List<String> stringList = new ArrayList<>();

    public StringJoiner() {
        delimiter = " ";
        prefix = "";
        suffix = "";
        result = "";
    }

    public StringJoiner append(String string) {
        stringList.add(string);
        return this;
    }

    public String build() {

        result = suffix;
        for (String string: stringList) {
            addBuildChain(string);
        }
        result += prefix;

        return result;
    }

    private void addBuildChain(String string) {
        result += string + delimiter;
    }
}